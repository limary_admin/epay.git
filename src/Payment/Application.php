<?php
namespace Sinta\Epay\Payment;

use Closure;
use Sinta\Epay\Kernel\Support;
use Sinta\Epay\Kernel\ServiceContainer;

/**
 * Class Application
 *
 * @see https://mch.swiftpass.cn/cms/sys/helpCenter/problemDetail?hcid=346
 * @https://open.swiftpass.cn/openapi/doc?index_1=3&index_2=1&chapter_1=191&chapter_2=212
 * @package Sinta\Epay\Payment
 */
class Application extends ServiceContainer
{

    protected $providers = [
        Order\ServiceProvider::class,
        Refund\ServiceProvider::class,
        Bill\ServiceProvider::class,
    ];


    protected $defaultConfig = [
        'http' => [
            'base_uri' => 'https://pay.swiftpass.cn/pay/gateway',
        ]
    ];


    /**
     * 微信的产品
     *
     * @param string $productId
     * @return string
     */
    public function scheme(string $productId): string
    {
        $params = [
            'appid' => $this['config']->app_id,
            'mch_id' => $this['config']->mch_id,
            'time_stamp' => time(),
            'nonce_str' => uniqid(),
            'product_id' => $productId,
        ];

        $params['sign'] = Support\generate_sign($params, $this['config']->key);

        return 'weixin://wxpay/bizpayurl?'.http_build_query($params);
    }



    /**
     * 处理支付通知
     *
     * @param Closure $closure
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handlePaidNotify(Closure $closure)
    {
        return (new Notify\Paid($this))->handle($closure);
    }

    /**
     * 处理退款通知
     *
     * @param Closure $closure
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleRefundedNotify(Closure $closure)
    {
        return (new Notify\Refunded($this))->handle($closure);
    }

    /**
     * 处理扫描通知
     *
     * @param Closure $closure
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleScannedNotify(Closure $closure)
    {
        return (new Notify\Scanned($this))->handle($closure);
    }


    public function __call($name,$arguments)
    {
        return call_user_func_array([$this['order'],$name],$arguments);
    }

}