<?php

namespace Sinta\Epay\Payment\Bill;

use Sinta\Epay\Kernel\Support;
use Sinta\Epay\Kernel\Exceptions\Exception;
use Sinta\Epay\Kernel\Http\StreamResponse;
use Sinta\Epay\Payment\Kernel\BaseClient;


/**
 * 下载对账单
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Bill
 */
class Client extends BaseClient
{
    const SERVICE_TYPE_MERCHANT = 'pay.bill.merchant';
    const SERVICE_TYPE_BIGMERCHANT = 'pay.bill.bigMerchant';
    const SERVICE_TYPE_AGENT = 'pay.bill.agent';

    public static $endpoints = [
        self::SERVICE_TYPE_MERCHANT,
        self::SERVICE_TYPE_BIGMERCHANT,
        self::SERVICE_TYPE_AGENT
    ];

    /**
     * 对账单
     *
     * @param $endpoint
     * @param string $date
     * @param string $type
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     * @throws Exception
     */
    public function download($endpoint,string $date,string $type = 'ALL')
    {
        $params = [
            'bill_date' => $date,
            'bill_type' => $type,
        ];

        if(!in_array($endpoint,static::$endpoints)){
            throw new Exception('接口类型参数值不正确！');
        }

        $response = $this->requestRaw($endpoint, $params);

        if(strpos($response->getBody()->getContents(),'<xml>') === 0){
            return $this->resolveResponse($response, $this->app['config']->get('response_type', 'array'));
        }

        return StreamResponse::buildFromPsrResponse($response);
    }


    protected function request(string $endpoint, array $params = [], $method = 'post',
                               array $options = [], $returnResponse = false)
    {
        $base = [
            'mch_id' => $this->app['config']['mch_id'],
            'nonce_str' => uniqid(),
            'service' => $endpoint,
        ];
        $params = array_filter(array_merge($base, $this->prepends(), $params));
        $params['sign'] = Support\generate_sign($params, $this->getKey($endpoint));

        $options = array_merge([
            'body' => Support\XML::build($params),
        ], $options);

        $response = $this->performRequest('', $method, $options);

        return $returnResponse
            ? $response
            : $this->resolveResponse($response, $this->app->config->get('response_type', 'array'));
    }
}