<?php
namespace Sinta\Epay\Payment\Bill;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 下载对账单服务
 *
 * Class ServiceProvider
 * @package Sinta\Epay\Payment\Bill
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['bill'] = function ($app){
            return new Client($app);
        };
    }
}