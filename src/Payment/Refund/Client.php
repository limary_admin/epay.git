<?php

namespace Sinta\Epay\Payment\Refund;


use Sinta\Epay\Payment\Kernel\BaseClient;

/**
 * 退款处理
 *
 * Class Client
 * @package Sinta\Epay\Payment\Refund
 */
class Client extends BaseClient
{
    const REFUND_QUERY_SERVICE_TYPE = 'unified.trade.refundquery';
    const REFUND_SERVICE_TYPE = 'unified.trade.refund';

    /**
     * 通过订单号发起退款
     *
     * @param string $number
     * @param string $refundNumber
     * @param int $totalFee
     * @param int $refundFee
     * @param array $optional
     * @return array|mixed|null|BaseClient|\Sinta\Epay\Payment\Kernel\ResponseInterface
     */
    public function byOutTradeNumber(string $number,string $refundNumber,int $totalFee, int $refundFee, array $optional = [])
    {
        return $this->refund($refundNumber, $totalFee, $refundFee, array_merge($optional, ['out_trade_no' => $number]));
    }

    /**
     * 通过交易号发起退款
     *
     * @param string $transactionId
     * @param string $refundNumber
     * @param int $totalFee
     * @param int $refundFee
     * @param array $optional
     * @return array|mixed|null|BaseClient|\Sinta\Epay\Payment\Kernel\ResponseInterface
     */
    public function byTransactionId(string $transactionId, string $refundNumber, int $totalFee, int $refundFee, array $optional = [])
    {
        return $this->refund($refundNumber, $totalFee, $refundFee, array_merge($optional, ['transaction_id' => $transactionId]));
    }

    /**
     * 通过交易号发起退款交易查询,平台订单号
     *
     * @param string $transactionId
     * @return array|mixed|null|\Sinta\Epay\Payment\Kernel\ResponseInterface|static
     */
    public function queryByTransactionId(string $transactionId)
    {
        return $this->query($transactionId, 'transaction_id');
    }

    /**
     * 通过订单号发起退款交易查询,商户订单号
     *
     * @param string $outTradeNumber
     * @return array|mixed|null|\Sinta\Epay\Payment\Kernel\ResponseInterface|static
     */
    public function queryByOutTradeNumber(string $outTradeNumber)
    {
        return $this->query($outTradeNumber, 'out_trade_no');
    }

    /**
     * 商户退款单号
     *
     * @param string $outRefundNumber
     * @return array|mixed|null|\Sinta\Epay\Payment\Kernel\ResponseInterface|Client
     */
    public function queryByOutRefundNumber(string $outRefundNumber)
    {
        return $this->query($outRefundNumber, 'out_refund_no');
    }

    /**
     * 通过平台退款单号查询
     *
     * @param string $refundId
     * @return array|mixed|null|\Sinta\Epay\Payment\Kernel\ResponseInterface|Client
     */
    public function queryByRefundId(string $refundId)
    {
        return $this->query($refundId, 'refund_id');
    }

    /**
     * 发起退款
     *
     *
     * @param string $refundNumber
     * @param int $totalFee
     * @param int $refundFee
     * @param array $optional
     * @return array|mixed|null|BaseClient|\Sinta\Epay\Payment\Kernel\ResponseInterface
     */
    protected function refund(string $refundNumber, int $totalFee, int $refundFee, $optional = [])
    {
        $params = array_merge([
            'out_refund_no' => $refundNumber,
            'total_fee' => $totalFee,
            'refund_fee' => $refundFee,
        ], $optional);

        return $this->safeRequest(self::REFUND_SERVICE_TYPE, $params);
    }

    /**
     * 退款交易查询
     *
     * @param string $number
     * @param string $type
     * @return array|mixed|null|\Sinta\Epay\Payment\Kernel\ResponseInterface|static
     */
    protected function query(string $number, string $type)
    {
        $params = [
            $type => $number,
        ];

        return $this->request(self::REFUND_QUERY_SERVICE_TYPE, $params);
    }
}