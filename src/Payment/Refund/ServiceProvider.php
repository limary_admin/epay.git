<?php

namespace Sinta\Epay\Payment\Refund;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
/**
 * 退款服务
 *
 * Class ServiceProvider
 * @see https://open.swiftpass.cn/openapi/doc?index_1=1&index_2=1&chapter_1=235&chapter_2=256
 * @package Sinta\Epay\Payment\Refund
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['refund'] = function ($app) {
            return new Client($app);
        };
    }

}