<?php

namespace Sinta\Epay\Payment\Kernel\Exceptions;

use Sinta\Epay\Kernel\Exceptions\Exception;

class InvalidSignException extends Exception
{

}