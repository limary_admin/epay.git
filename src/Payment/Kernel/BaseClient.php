<?php
namespace Sinta\Epay\Payment\Kernel;

use Sinta\Epay\Kernel\Support;
use Sinta\Epay\Payment\Application;
use Sinta\Epay\Kernel\Traits\HasHttpRequests;

/**
 * 请求客户端
 *
 * Class BaseClient
 * @package Sinta\Epay\Payment\Kernel
 */
class BaseClient
{
    use HasHttpRequests { request as performRequest; }

    protected $app;


    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->setHttpClient($this->app['http_client']);
    }

    /**
     * 预处理
     *
     * @return array
     */
    protected function prepends()
    {
        return [];
    }

    /**
     * 发送请求
     *
     * @param string $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @param bool $returnResponse
     * @return array|mixed|null|ResponseInterface|static
     */
    protected function request(string $endpoint, array $params = [], $method = 'post',
                               array $options = [], $returnResponse = false)
    {
        $base = [
            'appid' => $this->app['config']['app_id'],
            'mch_id' => $this->app['config']['mch_id'],
            'nonce_str' => uniqid(),
            'sub_mch_id' => $this->app['config']['sub_mch_id'],
            'sub_appid' => $this->app['config']['sub_appid'],
            'service' => $endpoint,
        ];
        $params = array_filter(array_merge($base, $this->prepends(), $params));
        $params['sign'] = Support\generate_sign($params, $this->getKey($endpoint));

        $options = array_merge([
            'body' => Support\XML::build($params),
        ], $options);

        $response = $this->performRequest('', $method, $options);

        return $returnResponse
            ? $response
            : $this->resolveResponse($response, $this->app->config->get('response_type', 'array'));
    }

    /**
     * 返回请求返回原示响应
     *
     * @param $endpoint
     * @param array $params
     * @param string $method
     * @param array $options
     * @return array|mixed|null|ResponseInterface|BaseClient
     */
    protected function requestRaw($endpoint, array $params = [], $method = 'post', array $options = [])
    {
        return $this->request($endpoint, $params, $method, $options, true);
    }


    /**
     * 安全请求
     *
     * @param $api
     * @param array $params
     * @param string $method
     * @return array|mixed|null|ResponseInterface|BaseClient
     */
    protected function safeRequest($endpoint, array $params, $method = 'post')
    {
        $options = [
            'cert' => $this->app['config']->get('cert_path'),
            'ssl_key' => $this->app['config']->get('key_path'),
        ];

        return $this->request($endpoint, $params, $method, $options);
    }

    /**
     * 获取商户密钥
     *
     * @param string $endpoint
     * @return mixed
     */
    protected function getKey(string $endpoint)
    {
        return $this->app['config']->key;
    }
}