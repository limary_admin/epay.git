<?php

namespace Sinta\Epay\Payment\Notify;

use Closure;
use Sinta\Epay\Kernel\Support\XML;

/**
 *退款通知处理
 *
 * Class Refunded
 * @package Sinta\Wechat\Payment\Notify
 */
class Refunded extends Handler
{
    protected $check = false;

    public function handle(Closure $closure)
    {
        $this->strict(
            $closure->bindTo($this)->__invoke($this->getMessage(), [$this, 'fail'])
        );

        return $this->toResponse();
    }


    public function reqInfo()
    {
        return XML::parse($this->decryptMessage('req_info'));
    }
}