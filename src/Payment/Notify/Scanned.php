<?php

namespace Sinta\Epay\Payment\Notify;

use Closure;

/**
 * 扫描通知处理
 *
 * Class Scanned
 * @package Sinta\Wechat\Payment\Notify
 */
class Scanned extends Handler
{

    protected $check = false;

    protected $alert;

    public function alert(string $message)
    {
        $this->alert = $message;
    }


    public function handle(Closure $closure)
    {
        $result = $closure->bindTo($this)->__invoke($this->getMessage(), [$this, 'fail'], [$this, 'alert']);

        $attributes = [
            'result_code' => is_null($this->alert) && is_null($this->fail) ? static::SUCCESS : static::FAIL,
            'err_code_des' => $this->alert,
        ];

        if (is_null($this->alert) && is_string($result)) {
            $attributes += [
                'appid' => $this->app['config']->app_id,
                'mch_id' => $this->app['config']->mch_id,
                'nonce_str' => uniqid(),
                'prepay_id' => $result,
            ];
        }

        return $this->respondWith($attributes, true)->toResponse();
    }
}