<?php

namespace Sinta\Epay\Payment\Order;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['bill'] = function ($app){
            return new Client($app);
        };
    }
}