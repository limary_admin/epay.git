<?php

namespace Sinta\Epay\Payment\Order;

use Sinta\Epay\Kernel\Support;
use Sinta\Epay\Payment\Kernel\BaseClient;

/**
 * 订单相关的方法
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Order
 */
class Client extends BaseClient
{

    /**
     * 服务接口
     */
    const PREPARE_SERVICE_TYPE =  'pay.weixin.jspay';
    const QUERY_SERVICE_TYPE = 'unified.trade.query';
    const CLOSE_SERVICE_TYPE = 'unified.trade.close';

    /**
     * 通一下订单
     *
     * @param array $order
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_1
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function unify(array $attributes)
    {
        if (empty($attributes['mch_create_ip'])) {
            $attributes['mch_create_ip'] = ($attributes['trade_type'] === 'NATIVE') ?
                Support\get_server_ip() : Support\get_client_ip();
        }
        $attributes['notify_url'] = $attributes['notify_url'] ?? $this->app['config']['notify_url'];

        return $this->request(self::PREPARE_SERVICE_TYPE, $attributes);
    }


    /**
     * 查询订单
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_2
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function query(array $params)
    {
        return $this->request(self::QUERY_SERVICE_TYPE,$params);
    }

    /**
     * 关闭订单
     *
     * @param string $tradeNo
     * @see https://pay.weixin.qq.com/wiki/doc/api/wxa/wxa_api.php?chapter=9_3
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function close(string $tradeNo)
    {
        $params = [
            'out_trade_no' => $tradeNo,
        ];

        return $this->request(self::CLOSE_SERVICE_TYPE, $params);
    }

    /**
     * 通过交号查询订单
     *
     * @param string $transactionId
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function queryByTransactionId(string $transactionId)
    {
        return $this->query([
            'transaction_id' => $transactionId,
        ]);
    }

    /**
     * 通过订单号查询订单
     *
     * @param string $number
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|Client
     */
    public function queryByOutTradeNumber(string $number)
    {
        return $this->query([
            'out_trade_no' => $number,
        ]);
    }

}