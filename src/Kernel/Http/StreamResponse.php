<?php
namespace Sinta\Epay\Kernel\Http;

use Sinta\Epay\Kernel\Support\File;

class StreamResponse extends Response
{
    public function save(string $directory, string $filename = '')
    {
        $this->getBody()->rewind();

        $directory = rtrim($directory, '/');

        if (!is_writable($directory)) {
            mkdir($directory, 0755, true); // @codeCoverageIgnore
        }

        $contents = $this->getBody()->getContents();

        if (empty($filename)) {
            $filename = md5($contents);
        }

        if (empty(pathinfo($filename, PATHINFO_EXTENSION))) {
            $filename .= File::getStreamExt($this->getBody());
        }

        file_put_contents($directory.'/'.$filename, $contents);

        return $filename;
    }

    public function saveAs(string $directory, string $filename)
    {
        return $this->save($directory, $filename);
    }
}