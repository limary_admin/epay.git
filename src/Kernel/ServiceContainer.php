<?php
namespace Sinta\Epay\Kernel;

use GuzzleHttp\Client as GHClient;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\HandlerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Pimple\Container;
use Symfony\Component\HttpFoundation\Request;

/**
 * 服务容器
 *
 * Class ServiceContainer
 * @package Sinta\Wechat\Kernel
 */
class ServiceContainer extends Container
{
    /**
     * 提供者
     *
     * @var array
     */
    protected $providers = [];

    /**
     * 默认设置
     *
     * @var array
     */
    protected $defaultConfig = [];


    /**
     * 全局设置
     *
     * @var array
     */
    protected $globalConfig = [
        'http' => [
            'timeout' => 5.0,
            'base_uri' => 'https://api.weixin.qq.com/',
        ],
    ];


    /**
     * 创一个服务容器
     *
     * ServiceContainer constructor.
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [],array $prepends = [])
    {
        parent::__construct($prepends);

        $this->registerConfig($config)
            ->registerProviders()
            ->registerLogger()
            ->registerRequest()
            ->registerHttpClient();
    }

    /**
     * 返回注册的服务
     *
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * 注册config
     *
     * @param array $config
     * @return $this
     */
    protected function registerConfig(array $config)
    {
        $this['config'] = function() use ($config){
            return new Config(
                array_replace_recursive($this->globalConfig, $this->defaultConfig, $config)
            );
        };
        return $this;
    }

    /**
     * 注册供应服务
     *
     * @return $this
     */
    protected function registerProviders()
    {
        foreach ($this->providers as $provider){
            $this->register(new $provider());
        }
        return $this;
    }

    /**
     * 注册请求对象
     *
     * @return $this
     */
    protected function registerRequest()
    {
        isset($this['request']) || $this['request'] = function (){
            return Request::createFromGlobals();
        };
        return $this;
    }

    /**
     * 注册日志
     *
     * @return $this
     */
    protected function registerLogger()
    {
        if (isset($this['logger'])) {
            return $this;
        }
        $logger = new Logger(str_replace('\\', '.', strtolower(get_class($this))));

        if ($logFile = $this['config']['log.file']) {
            $logger->pushHandler(new StreamHandler(
                    $logFile,
                    $this['config']->get('log.level', Logger::WARNING),
                    true,
                    $this['config']->get('log.permission', null))
            );
        } elseif ($this['config']['log.handler'] instanceof HandlerInterface) {
            $logger->pushHandler($this['config']['log.handler']);
        } else {
            $logger->pushHandler(new ErrorLogHandler());
        }

        $this['logger'] = $logger;


        return $this;
    }

    /**
     * 注册http client
     *
     * @return $this
     */
    protected function registerHttpClient()
    {
        isset($this['http_client']) || $this['http_client'] = function ($app) {
            return new GHClient($app['config']->get('http', []));
        };

        return $this;
    }

    public function __get($id)
    {
        return $this->offsetGet($id);
    }

    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }
}