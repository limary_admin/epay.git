<?php

namespace Sinta\Epay\Kernel\Support;

if(!function_exists('generate_sign')){
    /**
     * 产生签名
     *
     * @param array $attributes
     * @param $key
     * @param string $encryptMethod
     *
     * @return string
     */
    function generate_sign(array $attributes,$key,$encryptMethod = 'md5')
    {
        ksort($attributes);
        $attributes['key'] = $key;
        return strtoupper(call_user_func_array($encryptMethod,[http_build_query($attributes)]));
    }
}


if(!function_exists('get_client_ip')){

    /**
     * 客户IP
     *
     * @return string
     */
    function get_client_ip()
    {
        if(!empty($_SERVER['REMOTE_ADDR'])){
            $ip = $_SERVER['REMOTE_ADDR'];
        }else{
            $ip = defined('PHPUNIT_RUNNING') ? '127.0.0.1' : gethostbyname(gethostname());
        }
        return filter_var($ip, FILTER_VALIDATE_IP) ?: '127.0.0.1';
    }
}

if(!function_exists('get_server_ip')){
    /**
     * 服务IP
     *
     * @return string
     */
    function get_server_ip()
    {
        if (!empty($_SERVER['SERVER_ADDR'])) {
            $ip = $_SERVER['SERVER_ADDR'];
        } elseif (!empty($_SERVER['SERVER_NAME'])) {
            $ip = gethostbyname($_SERVER['SERVER_NAME']);
        } else {
            // for php-cli(phpunit etc.)
            $ip = defined('PHPUNIT_RUNNING') ? '127.0.0.1' : gethostbyname(gethostname());
        }

        return filter_var($ip, FILTER_VALIDATE_IP) ?: '127.0.0.1';
    }
}


if(!function_exists('current_url')){
    /**
     * 当前url
     *
     * @return string
     */
    function current_url()
    {
        $protocol = 'http://';

        if (!empty($_SERVER['HTTPS']) || ($_SERVER['HTTP_X_FORWARDED_PROTO'] ?? 'http') === 'https') {
            $protocol = 'https://';
        }

        return $protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
}