<?php
namespace Sinta\Epay\Kernel\Exceptions;

use Exception as BaseException;

/**
 * 基础异常
 *
 * Class Exception
 * @package Sinta\Wechat\Kernel\Exceptions
 */
class Exception extends BaseException
{

}