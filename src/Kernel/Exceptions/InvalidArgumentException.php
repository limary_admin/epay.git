<?php
namespace Sinta\Epay\Kernel\Exceptions;

/**
 * 无效参数异常
 *
 * Class InvalidArgumentException
 * @package Sinta\Wechat\Kernel\Exceptions
 */
class InvalidArgumentException extends Exception
{

}