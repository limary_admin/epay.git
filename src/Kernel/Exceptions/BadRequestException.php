<?php
namespace Sinta\Epay\Kernel\Exceptions;

/**
 * Class BadRequestException
 * @package Sinta\Wechat\Kernel\Exceptions
 */
class BadRequestException extends Exception
{

}